﻿#include <iostream>

void printNumbers(int n, bool isOdd) {
	for (size_t i = isOdd; i < n; i += 2)
	{
		std::cout << i << std::endl;
	}
}

int main()
{
	int n;
	std::cout << "Enter the N: ";
	std::cin >> n;

	printNumbers(n, true);
}